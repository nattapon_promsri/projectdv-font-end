export const setToken = takeToken => ({
   
    type: 'SET_TOKEN',
    token: takeToken
    
})
export const setUser = takeUser => ({

    type: 'SET_ROLE',
    user: takeUser
})
