import axios from "axios";
import { setToken } from "../action/loginAction";

export const host = "https://nattapon.crabdance.com"; 


// http://(ip address ของเน็ตที่ต่อ):8080 โทรสัพต้องต่อวายไฟอันเดียวกันด้วย


export function signUp(firstname, lastname, username, password, role) {

    return axios.post(`${host}/signup`, { firstname, lastname, username, password, role })
}

export function getUserData(token) {

    return axios.get(`${host}/getUserData`, {
        headers:
        {
            Authorization: `Bearer ${token}`
        }
    })
}

export function maintenaceList() {
    return axios.get(`${host}/maintenace`);
}

export function getListContent(maintenace_id) {
    return axios.get(`${host}/content/${maintenace_id}`);
}

export function postContent(maintenace_id, { head, description }, token) {
    return axios.post(`${host}/content/${maintenace_id}`, { head, description }, {
        headers:
        {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getListComment(content_id) {
    return axios.get(`${host}/comment/${content_id}`);
}


export function postComment(content_id, comment, token) {
    return axios.post(`${host}/comment/${content_id}`, comment, {
        headers:
        {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getGarage() {
    return axios.get(`${host}/garage`);
}


export function postGarage({ name, description, latitude, longitude }, token) {

    const data = {
        "name": name,
        "description": description,
        "latitude": latitude.toString(),
        "longitude": longitude.toString(),

    }

    return axios.post(`${host}/garage`, data, {
        headers:
        {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getOneGarage(token) {
    return axios.get(`${host}/getOneGarage` ,{
        headers:
        {
            Authorization: `Bearer ${token}`
        }
    })
}

