import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, ScrollView } from 'react-native'
import { getListComment, postComment } from '../api';
import { Card, Title, TextInput, Button, IconButton, Paragraph } from 'react-native-paper';
import { useSelector } from 'react-redux';
import { Header, Left, Body, Right, } from 'native-base';


export default function Comment(props) {

    const [comments, setComments] = useState([])
    const [comment, setComment] = useState("")
    const token = useSelector(state => state.token)

    const contentID = props.id

    useEffect(() => {
        fetchData()
    }, [new Date().getMinutes()])

    const fetchData=()=>{
        console.log("props123123.: ", props);
        
        getListComment(props.id)
        .then(res => {
            console.log("resssss", res);
            
            setComments(res.data)
            console.log("SUCCESS");
        })
        .catch(err => {
            console.log("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR", err);
        })
    }
    
    const onSubmit = () => {
        postComment(contentID, { comment: comment }, token)
            .then(res => {
                console.log(res)
                setComment("")
                setTimeout(() => {
                    
                    fetchData()
                }, 1000);
            })
            .catch(err => {
                console.log("err : ", err);
            })
    }
    return (
        <View >
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>

                <FlatList
                    data={comments}
                    renderItem={({ item }) => (
                        <View style={{ margin: 5 }}>
                            <View >
                                <Card>
                                    <Card.Content>
                                        <Title style={{ fontSize: 14 }}>{item.Firstname} {item.Lastname}</Title>
                                        <Paragraph style={{ fontSize: 12 }}>{item.Comment}</Paragraph>
                                    </Card.Content>
                                </Card>
                            </View>
                        </View>
                    )}
                />

            </ScrollView>
            {token !== null ?
                <View style={{ alignItems: "center", margin: 10, flexDirection: "row" }}>
                    <TextInput
                        style={{ width: 320, marginVertical: 10, flex: 2 }}
                        label='Comment'
                        mode='outlined'
                        selectionColor='pink'
                        value={comment}
                        onChangeText={(value) => setComment(value)}
                    />
                    <Button style={{ height: 50, flex: 0.1 }}
                        color="black"
                        mode="contained"
                        onPress={() => onSubmit()}>
                        <Text style={{ fontSize: 16 }}>Post</Text>
                    </Button>
                </View>
                : <View></View>
            }

        </View>


    )
}