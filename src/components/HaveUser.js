import React, { useState, useEffect } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { setToken, setUser } from '../action/loginAction'
import { ScrollView } from 'react-native-gesture-handler'
import { useDispatch, useSelector } from 'react-redux'
import { getUserData } from '../api'
import { Card, Button } from 'react-native-paper';
import { Header, Left, Body, Right, } from 'native-base';

export default function HaveUser(props) {

    const [userData, setUserData] = useState([])
    const token = useSelector(state => state.token)
    const [status, setStatus] = useState("")

    const dispatch = useDispatch()

    useEffect(() => {
        getUserData(token)
            .then(res => {
                setUserData(res.data)
                dispatch(setUser(res.data))
                if (res.data.role == true) {
                    setStatus("Garage Owner")
                } else {
                    setStatus("User")
                }
            })
            .catch(err => {
                alert("Error : ", err)
            })
    }, []);

    const logOut = () => {

        dispatch(setToken(null))
        props.navigation.navigate('Signin')

    }

    return (


        <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
            <View style={{ alignItems: 'center' }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold', margin: 20 }}>
                    My Profile
                        </Text>
            </View>

            <View style={{ alignItems: 'center', marginBottom: 50 }}>
                <Image
                    style={{ height: 150, width: 150, borderRadius: 50 }}
                    source={require('../assets/face.png')}
                />
            </View>

            <View style={{ backgroundColor: 'grey', marginTop: 10, margin: 15, borderRadius: 50 }}>
                <View style={{ justifyContent: 'center', marginLeft: 10, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 10 }}>
                        Name :
                        </Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 10 }}>
                        {userData.firstname}
                    </Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 10 }}>
                        {userData.lastname}
                    </Text>
                </View>
                <View style={{ justifyContent: 'center', marginLeft: 10, flexDirection: 'row' }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 10 }}>
                        Status :
                        </Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 10 }}>
                        {status}
                    </Text>
                </View>
            </View>
            {
                userData.role == true ?
                    <View>
                        <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                            <Button
                                color="#EE5FC3"
                                onPress={() => props.navigation.navigate('AddYourGarage')}
                            >
                                Add Your Garage
                             </Button>
                        </View>
                        <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                            <Button
                                color="#EE5FC3"
                                onPress={() => props.navigation.navigate('YourGarage')}
                            >
                                Your Garage
                            </Button>

                        </View>
                    </View>
                    :
                    <View></View>
            }
            <View>
                <Button
                    color="red"
                    onPress={() => logOut()}
                >
                    Log Out
                </Button>
            </View>
        </ScrollView>

    )
}
