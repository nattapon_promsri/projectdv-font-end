import React from 'react'
import { View, Text } from 'react-native'
import { IconButton, Button } from 'react-native-paper';
import { Header, Left, Body, Right, } from 'native-base';

export default function NoUser(props) {


    return (

        <View>
            <View style={{ alignItems: "center" }}>

                <IconButton
                    icon="account-alert-outline"
                    color="black"
                    size={200}
                />
                <Text style={{ color: 'white', fontSize: 20 }}>Doesn't exist user</Text>
                <Text style={{ color: 'white', fontSize: 20 }}>Please Sign in first</Text>
            </View>
            <View style={{ alignItems: "center", margin: 50 }}>
                <Button icon="account-arrow-right" color="black" mode="contained" onPress={() => props.navigation.navigate('Signin')}>
                    Sign in
                </Button>
            </View>
        </View>

    )
}