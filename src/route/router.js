import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';
import { createStore } from 'redux'
import { loginReducer } from '../reducer/loginReducer';
import Home from '../screens/Home';
import Maintenace from '../screens/Maintenace';
import Profile from '../screens/Profile';
import Signup from '../screens/Signup';
import Signin from '../screens/Signin';
import tabBar from '../route/tabBar'
import NoUser from '../components/NoUser';
import HaveUser from '../components/HaveUser';
import AddYourGarage from '../screens/AddYourGarage';
import YourGarage from '../screens/YourGarage';
import Content from '../screens/Content';
import Comment from '../components/Comment';
import ContentDescription from '../screens/ContentDescription';
import AddContent from '../screens/AddContent';
import ShowMap from '../screens/ShowMap';

const store = createStore(loginReducer)
const Stack = createStackNavigator();

export default function router() {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{ headerShown: false }}
                >
                    <Stack.Screen name="TabBar" component={tabBar} exact={true}  />
                    <Stack.Screen name="Signin" component={Signin} />
                    <Stack.Screen name="Signup" component={Signup} />
                    <Stack.Screen name="Home" component={Home} />
                    <Stack.Screen name="Maintenace" component={Maintenace} />
                    <Stack.Screen name="Profile" component={Profile} />
                    <Stack.Screen name="NoUser" component={NoUser} />
                    <Stack.Screen name="HaveUser" component={HaveUser} />
                    <Stack.Screen name="AddYourGarage" component={AddYourGarage} />
                    <Stack.Screen name="YourGarage" component={YourGarage} />
                    <Stack.Screen name="Content" component={Content} />
                    <Stack.Screen name="AddContent" component={AddContent} />
                    <Stack.Screen name="ContentDescription" component={ContentDescription} />
                    <Stack.Screen name="Comment" component={Comment} />
                    <Stack.Screen name="ShowMap" component={ShowMap} />

                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    )
}
