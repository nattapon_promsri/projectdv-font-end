import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { IconButton } from 'react-native-paper';
import Home from '../screens/Home';
import Maintenace from '../screens/Maintenace';
import Profile from '../screens/Profile';

const Tab = createBottomTabNavigator();


export default function tabBar() {
    return (
            <Tab.Navigator
                initialRouteName="Home"
                tabBarOptions={{
                    activeTintColor: 'blue',
                }}
            >
                <Tab.Screen
                    name="Maintenace"
                    component={Maintenace}
                    options={{
                        tabBarLabel: 'Maintenace',
                        tabBarIcon: () => (
                            <IconButton
                                icon="wrench"
                                color="black"
                                size={32}
                            />
                        ),
                    }}
                />
                <Tab.Screen

                    name="Home"
                    component={Home}
                    style={{ backgroundColor: 'blue' }}
                    options={{
                        tabBarLabel: 'Find Garage',
                        tabBarIcon: () => (
                            <IconButton
                                icon="map-marker-radius"
                                color="black"
                                size={32}
                            />
                        ),

                    }}
                />
                <Tab.Screen
                    name="Profile"
                    component={Profile}
                    options={{
                        tabBarLabel: 'Profile',
                        tabBarIcon: () => (
                            <IconButton
                                icon="account"
                                color="black"
                                size={32}
                            />
                        ),
                    }}
                />
            </Tab.Navigator>
    )
}