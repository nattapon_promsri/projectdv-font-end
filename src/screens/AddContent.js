import React, { useState } from 'react'
import { View, Text, ImageBackground } from 'react-native'
import { TextInput, IconButton, Button } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { postContent } from '../api';
import { Header, Left, Body, Right } from 'native-base';


export default function AddContent(props) {

    const [head, setHaed] = useState("")
    const [description, setDescription] = useState("")

    const token = useSelector(state => state.token)
    const maintenace_id = props.route.params.id

    const onSubmit = () => {
        postContent(maintenace_id, { head, description }, token)
            .then(res => {
                props.navigation.navigate('Content', { fetchData: true })
            })
            .catch(err => {
                console.log(err);

            })
    }

    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9" }}>
            <ImageBackground source={require('../assets/bg4.png')} style={{ height: 620, width: 370 }}>
                <Header style={{ backgroundColor: "white" }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack()}
                        >
                            <IconButton
                                icon="arrow-left-bold"
                                color="black"
                                size={40}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ alignItems: "center" }}>
                        <Text ></Text>
                    </Body>
                    <Right>

                    </Right>
                </Header>
                <ScrollView>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 20 }}>
                            Add Your Content
            </Text>
                    </View>
                    <View style={{ alignItems: "center", marginBottom: 10 }}>
                        <TextInput
                            style={{ width: 300 }}
                            label='Header'
                            mode='outlined'
                            selectionColor='pink'
                            value={head}
                            onChangeText={(value) => setHaed(value)}
                        />
                        <TextInput
                            style={{ width: 300 }}
                            label='Description'
                            mode='outlined'
                            selectionColor='pink'
                            value={description}
                            onChangeText={(value) => setDescription(value)}
                        />
                    </View>

                    <View style={{ alignItems: "center" }}>
                        <View style={{ margin: 10 }}>
                            <Button icon="plus-box-outline" color="black" mode="contained" onPress={() => onSubmit()}>
                                Add
                        </Button>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    )
}