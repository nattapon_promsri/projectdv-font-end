import React, { useState, useRef, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { TextInput, Button, IconButton } from 'react-native-paper';
import { useSelector } from 'react-redux';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import Geolocation from 'react-native-geolocation-service'
import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions'
import { postGarage } from '../api';
import { Header, Left, Body, Right, } from 'native-base';



export default function AddYourGarage(props) {
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")

    const [coordinate, setCoordinate] = useState({
        latitude: 37.78825,
        longitude: -122.4324,
    })

    const token = useSelector(state => state.token)

    const MapViewRef = useRef()


    useEffect(() => {

        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then(result => {
                if (result === RESULTS.GRANTED) {
                    callLocation((position) => {
                        setCoordinate(position.coords)
                        MapViewRef.current.animateCamera({
                            center: {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                            }
                        })
                    })
                }
                if (result === RESULTS.DENIED) {
                    request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                        .then(result => {
                            if (result === RESULTS.GRANTED) {
                                callLocation((position) => {
                                    setCoordinate(position.coords)
                                    MapViewRef.current.animateCamera({
                                        center: {
                                            latitude: position.coords.latitude,
                                            longitude: position.coords.longitude,
                                        }
                                    })
                                })
                            }
                            else if (result === RESULTS.DENIED) {
                                alert('Please accept')
                            }
                            else if (result === RESULTS.BLOCKED) {
                                alert('Thank you')
                            }
                        })
                    return
                }
                if (result === RESULTS.BLOCKED) {
                    alert('...')
                }
            })
    }, [])

    const callLocation = (callback) => {
        Geolocation.getCurrentPosition(
            (position) => {
                callback(position)
            },
            (error) => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        )
    }
    const addGarage = () => {

        postGarage({ name, description, latitude: coordinate.latitude, longitude: coordinate.longitude }, token)
            .then(res => {
                props.navigation.navigate('TabBar')
            })
            .catch()
    }


    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9" }}>
            <ImageBackground source={require('../assets/bg3.png')} style={{ height: 620, width: 370 }}>
                <Header style={{ backgroundColor: "white" }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack()}
                        >
                            <IconButton
                                icon="arrow-left-bold"
                                color="black"
                                size={40}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ alignItems: "center" }}>
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>Add Your Garage</Text>
                    </Body>
                    <Right>

                    </Right>
                </Header>
                <ScrollView>
                    <View style={{ alignItems: "center", margin: 10 }}>
                        <TextInput
                            style={{ width: 300 }}
                            label='Name'
                            mode='outlined'
                            selectionColor='pink'
                            value={name}
                            onChangeText={(value) => setName(value)}
                        />
                        <TextInput
                            style={{ width: 300 }}
                            label='Description'
                            mode='outlined'
                            selectionColor='pink'
                            value={description}
                            onChangeText={(value) => setDescription(value)}
                        />
                    </View>
                    <View>
                        <View style={{
                            height: 300,
                            width: 320,
                            margin: 20
                        }}>
                            <MapView
                                ref={MapViewRef}
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={styles.map}
                                region={{
                                    latitude: coordinate.latitude,
                                    longitude: coordinate.longitude,
                                    latitudeDelta: 0.015,
                                    longitudeDelta: 0.0121,
                                }}
                            >
                                <Marker draggable
                                    coordinate={coordinate}
                                    onDragEnd={(event) => setCoordinate(event.nativeEvent.coordinate)}
                                />
                            </MapView>
                        </View>
                    </View>
                    <View style={{ alignItems: "center" }}>
                        <View style={{ margin: 10 }}>
                            <Button icon="plus-box-outline" color="black" mode="contained" onPress={() => addGarage()}>
                                Add Garage
                        </Button>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View >
    )
}
const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
