import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, TouchableOpacity, ScrollView, ImageBackground } from 'react-native'
import { getListContent } from '../api';
import { Card, Title, Button, IconButton } from 'react-native-paper';
import { useSelector } from 'react-redux';
import { Header, Left, Body, Right, } from 'native-base';
import { useFocusEffect } from '@react-navigation/native';


export default function Content(props) {

    const [content, setContent] = useState([])
    const token = useSelector(state => state.token)

    const maintenace_id = props.route.params.item

    useEffect(() => {
        getContent()
    }, []);

    const getContent = () => {
        getListContent(maintenace_id.ID)
            .then(res => {
                console.log("res.data: ", res.data)
                setContent(res.data)
            })
            .catch(err => {
                alert("Error : ", err)
            })
    }

    useFocusEffect(
        React.useCallback(() => {
            getContent()
        }, [])
    );

    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9" }}>
            <ImageBackground source={require('../assets/bg3.png')} style={{ height: 620, width: 370 }}>
                <Header style={{ backgroundColor: "white" }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack()}
                        >
                            <IconButton
                                icon="arrow-left-bold"
                                color="black"
                                size={40}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ alignItems: "center" }}>
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>Contents</Text>
                    </Body>
                    <Right>
                        {
                            token !== null ?
                                <View style={{ width: 100 }}>
                                    <Button icon="plus-box-outline" color="black" mode="contained" onPress={() => props.navigation.navigate('AddContent', { id: maintenace_id.ID })}>Add</Button>
                                </View>
                                : <View></View>
                        }
                    </Right>
                </Header>
                <View>

                </View>

                <ScrollView>
                    <View>
                        <FlatList
                            data={content}
                            renderItem={({ item }) => (
                                <View style={{ margin: 10 }}>
                                    {console.log("itemmmmmm: ", item)}
                                    <TouchableOpacity
                                        // key={item.key}
                                        onPress={() => props.navigation.navigate('ContentDescription', { item })}
                                    >
                                        <View>
                                            <Card>
                                                <Card.Content>
                                                    <Title>{item.head}</Title>
                                                </Card.Content>
                                            </Card>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>


    )
}