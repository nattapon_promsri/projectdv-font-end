import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import { maintenaceList } from '../api';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { Card, Title, Paragraph, IconButton } from 'react-native-paper';
import Comment from '../components/Comment';
import { Header, Left, Body, Right, } from 'native-base';

export default function ContentDescription(props) {

    const [content, setContent] = useState([])

    const contentItem = props.route.params.item

    useEffect(() => {
        setContent(contentItem)
    }, []);


    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9" }}>
            <ImageBackground source={require('../assets/bg3.png')} style={{ height: 620, width: 370 }}>
                <Header style={{ backgroundColor: "white", marginBottom: 10 }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack()}
                        >
                            <IconButton
                                icon="arrow-left-bold"
                                color="black"
                                size={40}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ alignItems: "center" }}>
                        <Text ></Text>
                    </Body>
                    <Right>

                    </Right>
                </Header>
                <View >
                    <Card>
                        <Card.Content>
                            <Title style={{ fontSize: 24, fontWeight: "bold", marginBottom: 20 }}> {content.head}</Title>
                            <Paragraph style={{ fontSize: 18 }}>{content.description}</Paragraph>
                        </Card.Content>
                    </Card>
                </View>
                <View style={{ margin: 10 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Comments...</Text>
                </View>
                <ScrollView>
                    <Comment navigation={props.navigation} id={contentItem.ID} />
                </ScrollView>
            </ImageBackground>
        </View>

    )
}