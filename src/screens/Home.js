import React from 'react'
import { View, Text, TouchableOpacity, Image, ImageBackground } from 'react-native'

export default function Home(props) {

    return (
        <View style={{ backgroundColor: "#ABB2B9", flex: 1, justifyContent: 'center', alignItems: "center" }} >
            <ImageBackground source={require('../assets/bg4.png')} style={{ height: 620, width: 370 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 150 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('ShowMap')}>
                        <Image
                            style={{ height: 200, width: 200 }}
                            source={require('../assets/fix2.png')}
                        />
                        <View style={{ marginTop: 20, alignItems: 'center' }}>
                            <Text style={{ fontSize: 24, color: 'white' }}>Press Here</Text>
                        </View>
                    </TouchableOpacity>

                </View>

            </ImageBackground>
        </View>

    )
}