import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, TouchableOpacity, ScrollView, ImageBackground } from 'react-native'
import { maintenaceList } from '../api';
import { Card, Title } from 'react-native-paper';
import { Header, Left, Body, Right, } from 'native-base';

export default function Maintenace(props) {

    const [maintenace, setMaintenace] = useState([])

    useEffect(() => {
        maintenaceList()
            .then(res => {
                setMaintenace(res.data)
            })
            .catch(err => {
                alert("Error : ", err)
            })
    }, []);
    // props.naviv.nat('content',{id:item.id})

    return (

        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#ABB2B9" }}>
            <ImageBackground source={require('../assets/bg4.png')} style={{ height: 620, width: 370 }}>
                <ScrollView>
                    <View style={{ flexDirection: "row", marginTop: 50 }}>
                        <FlatList
                            data={maintenace}
                            numColumns={2}
                            renderItem={({ item }) => (
                                <View style={{ margin: 10 }}>
                                    <TouchableOpacity
                                        key={item.key}
                                        onPress={() => props.navigation.navigate('Content', { item })}
                                    >
                                        <View style={{ height: 150, width: 160, marginBottom: 90 }}>
                                            <Card style={{ borderWidth: 0 }}>
                                                <Card.Content style={{ borderBottomWidth: 1, backgroundColor: "#ABB2B9" }}>
                                                    <Title style={{ fontSize: 20, fontWeight: 'bold', fontStyle: 'italic' }}>{item.Name}</Title>

                                                </Card.Content>
                                                <Card.Cover source={{ uri: item.Photo }} />
                                            </Card>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>

    )
}