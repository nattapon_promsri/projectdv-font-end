import React, { useState } from 'react'
import { View, Text, Button, ImageBackground } from 'react-native'
import { useSelector } from 'react-redux'
import NoUser from '../components/NoUser'
import HaveUser from '../components/HaveUser'
import { Header, Left, Body, Right, } from 'native-base';

export default function Profile(props) {

    const token = useSelector(state => state.token)

    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9", justifyContent: 'center', alignItems: "center" }}>
            <ImageBackground source={require('../assets/bg4.png')} style={{ height: 620, width: 370 }}>
                {
                    token !== null ?
                        <HaveUser navigation={props.navigation} />
                        : <NoUser navigation={props.navigation} />
                }
            </ImageBackground>

        </View>
    )
}


//display : none ใน style       

