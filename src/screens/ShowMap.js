import React, { useState, useEffect, useRef } from 'react'
import { View, StyleSheet, Dimensions, Text, SafeAreaView, TouchableOpacity  } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service'
import { check, request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions'
import { getGarage } from '../api';
import Carousel from 'react-native-snap-carousel';
import { Button, IconButton } from 'react-native-paper';
import getDirections from 'react-native-google-maps-directions'
import { Header, Left, Body, Right, } from 'native-base';



export default function ShowMap(props) {

    const [allGarage, setAllGarage] = useState([])
    const [coordinate, setCoordinate] = useState({
        latitude: 0,
        longitude: 0,
    })

    useEffect(() => {

        /////////////////check premision////////////////
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then(result => {
                if (result === RESULTS.GRANTED) {
                    callLocation((position) => {
                        setCoordinate(position.coords)
                        MapViewRef.current.animateCamera({
                            center: {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                            }

                        })
                    })
                }
                if (result === RESULTS.DENIED) {
                    request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                        .then(result => {
                            if (result === RESULTS.GRANTED) {
                                callLocation((position) => {
                                    setCoordinate(position.coords)
                                    MapViewRef.current.animateCamera({
                                        center: {
                                            latitude: position.coords.latitude,
                                            longitude: position.coords.longitude,
                                        }
                                    })
                                })
                            }
                            else if (result === RESULTS.DENIED) {
                                alert('Please accept')
                            }
                            else if (result === RESULTS.BLOCKED) {
                                alert('Thank you')
                            }
                        })
                    return
                }
                if (result === RESULTS.BLOCKED) {
                    alert('...')
                }
            })
        ///////////////////////////////////

        getGarage()
            .then(res => {
                setAllGarage(res.data)
            })
            .catch(err => {
                console.log("err : ", err);
            })
    }, [])


    const callLocation = (callback) => {
        Geolocation.getCurrentPosition(
            (position) => {
                callback(position)
            },
            (error) => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        )
    }

    const renderCarouselItem = ({ item }) =>
        <View style={styles.cardContainer}>
            <Text style={styles.cardTitle}>{item.name}</Text>
            <Text style={styles.cardTitle}>{item.description}</Text>
            <Button icon="car" color="black" mode="contained" style={{ marginTop: 10 }} onPress={() => handleGetDirections({ latitude: item.latitude, longitude: item.longitude })}>
                Get direction
            </Button>
        </View>

    const onCarouselItemChange = (index) => {
        let location = allGarage[index];
        MapViewRef.current.animateToRegion({
            latitude: parseFloat(location.latitude),
            longitude: parseFloat(location.longitude),
            latitudeDelta: 0.09,
            longitudeDelta: 0.035
        })
    }

    const handleGetDirections = (item) => {

        console.log("asdasdasdasd", item);

        console.log("asdfasdfasdf", coordinate);



        const data = {
            source: {
                latitude: coordinate.latitude,
                longitude: coordinate.longitude
            },
            destination: {
                latitude: parseFloat(item.latitude),
                longitude: parseFloat(item.longitude)
            },
            params: [
                {
                    key: "travelmode",
                    value: "driving"        // may be "walking", "bicycling" or "transit" as well
                },
                {
                    key: "dir_action",
                    value: "navigate"       // this instantly initializes navigation using the given travel mode
                }
            ]
        }
        getDirections(data)
    }


    const MapViewRef = useRef()

    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9" }}>
            <Header style={{ backgroundColor: "white" }}>
                <Left>
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}
                    >
                        <IconButton
                            icon="arrow-left-bold"
                            color="black"
                            size={40}
                        />
                    </TouchableOpacity>
                </Left>
                <Body style={{ alignItems: "center" }}>
                    <Text style={{ fontWeight: "bold", fontSize: 18 }}>Maps</Text>
                </Body>
                <Right>

                </Right>
            </Header>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                <SafeAreaView>
                    <View style={{
                        height: "96%",
                        width: 350
                    }}>
                        <MapView
                            ref={MapViewRef}
                            provider={PROVIDER_GOOGLE}
                            style={styles.map}
                            region={{
                                latitude: coordinate.latitude,
                                longitude: coordinate.longitude,
                                latitudeDelta: 0.015,
                                longitudeDelta: 0.0121,
                            }}
                        >
                            {
                                allGarage.map((item, index) => {
                                    return <Marker
                                        image={require('../assets/car.png')}

                                        coordinate={{ latitude: parseFloat(item.latitude), longitude: parseFloat(item.longitude) }}
                                    >
                                        <Callout>
                                            <Text>{item.name}</Text>
                                        </Callout>
                                    </Marker>
                                })
                            }

                            <Marker
                                image={require('../assets/user.png')}
                                coordinate={coordinate}
                            >
                            </Marker>
                        </MapView>
                        <Carousel
                            data={allGarage}
                            containerCustomStyle={styles.carousel}
                            renderItem={(item) => renderCarouselItem(item)}
                            sliderWidth={Dimensions.get('window').width}
                            itemWidth={300}
                            removeClippedSubviews={false}
                            onSnapToItem={(index) => onCarouselItemChange(index)}
                        />

                    </View>
                </SafeAreaView>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: 150,
        width: 300,
        padding: 24,
        borderRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 16,
        alignSelf: 'center'
    }
});