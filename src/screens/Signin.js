import React, { useState } from 'react'
import { View, Text, ImageBackground } from 'react-native'
import { TextInput, IconButton, Button } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch } from "react-redux";
import { host } from '../api';
import axios from 'axios';
import { setToken } from '../action/loginAction';
import { Header, Left, Body, Right, } from 'native-base';


export default function Signin(props) {

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const dispatch = useDispatch()

    const signin = () => {
        axios.post(`${host}/signin`, { username, password })
            .then(res => {
                dispatch(setToken(res.data.token))
                props.navigation.navigate('TabBar')
            })
            .catch(err => {
                console.log("ERROR : ", err)
            })

    }

    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9" }}>
            <ImageBackground source={require('../assets/bg4.png')} style={{ height: 620, width: 370 }}>
                <Header style={{ backgroundColor: "white" }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack()}
                        >
                            <IconButton
                                icon="arrow-left-bold"
                                color="black"
                                size={40}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ alignItems: "center" }}>
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>Sign In</Text>
                    </Body>
                    <Right>

                    </Right>
                </Header>
                <ScrollView>
                    <View style={{ alignItems: "center"}}>
                        <IconButton
                            icon="account-box"
                            color="#FDFEFE"
                            size={200}
                        />
                    </View>
                    <View style={{ alignItems: "center", marginBottom: 10 }}>
                        <TextInput
                            style={{ width: 300 }}
                            label='Username'
                            mode='outlined'
                            selectionColor='pink'
                            value={username}
                            onChangeText={(value) => setUsername(value)}
                        />
                        <TextInput
                            style={{ width: 300 }}
                            label='Password'
                            mode='outlined'
                            selectionColor='pink'
                            secureTextEntry={true}
                            value={password}
                            onChangeText={(value) => setPassword(value)}
                        />
                    </View>
                    <View style={{ justifyContent: "center", flexDirection: "row" }}>
                        <View>
                            <Text style={{ color: 'white' }}>Doesn't have an account ?</Text>
                        </View>

                        <View style={{ marginHorizontal: 10 }}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('Signup')}>
                                <Text style={{ fontWeight: "bold", color: 'white' }}>Sign up</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                    <View style={{ alignItems: "center" }}>
                        <View style={{ margin: 10 }}>
                            <Button icon="account-arrow-right" color="black" mode="contained" onPress={() => signin()}>
                                Sign in
                        </Button>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>

    )
}