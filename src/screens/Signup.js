import React, { useState } from 'react'
import { View, Text, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { TextInput, IconButton, Button, Checkbox } from 'react-native-paper';
import { signUp } from '../api';
import { Header, Left, Body, Right, } from 'native-base';

export default function Signup(props) {

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [firstname, setFirstname] = useState("")
    const [lastname, setLastname] = useState("")
    const [role, setRole] = useState(false)

    const [checkbox, setCheckbox] = useState("unchecked")

    const onCheckbox = () => {
        checkbox === "checked" ? "unchecked" : "checked"
        if (checkbox === "checked") {
            setCheckbox("unchecked")
            setRole(false)
        } else if (checkbox === "unchecked") {
            setCheckbox("checked")
            setRole(true)
        }
    }

    const onSubmit = () => {
        signUp(firstname, lastname, username, password, role)
            .then(res => {
                props.navigation.navigate('Signin')
            })
            .catch(err => {
                alert("Error : ", err)
            })
    }
    return (
        <View style={{ backgroundColor: "#ABB2B9", flex: 1 }}>
            <ImageBackground source={require('../assets/bg4.png')} style={{ height: 620, width: 370 }}>
                <Header style={{ backgroundColor: "white" }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack()}
                        >
                            <IconButton
                                icon="arrow-left-bold"
                                color="black"
                                size={40}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ alignItems: "center" }}>
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>Sign Up</Text>
                    </Body>
                    <Right>

                    </Right>
                </Header>

                <ScrollView>
                    <View style={{ alignItems: "center", marginBottom: 10 }}>
                        <Text style={{ fontWeight: "bold", fontSize: 36 }}>
                            Sign Up
                    </Text>
                    </View>
                    <View style={{ alignItems: "center", marginBottom: 10 }}>
                        <TextInput
                            style={{ width: 320, marginVertical: 10 }}
                            label='Username'
                            mode='outlined'
                            selectionColor='pink'
                            value={username}
                            onChangeText={(value) => setUsername(value)}
                        />
                        <TextInput
                            style={{ width: 320, marginVertical: 10 }}
                            label='Password'
                            mode='outlined'
                            selectionColor='pink'
                            secureTextEntry={true}
                            value={password}
                            onChangeText={(value) => setPassword(value)}
                        />
                        <TextInput
                            style={{ width: 320, marginVertical: 10 }}
                            label='Firstname'
                            mode='outlined'
                            selectionColor='pink'
                            value={firstname}
                            onChangeText={(value) => setFirstname(value)}
                        />
                        <TextInput
                            style={{ width: 320, marginVertical: 10 }}
                            label='Lastname'
                            mode='outlined'
                            selectionColor='pink'
                            value={lastname}
                            onChangeText={(value) => setLastname(value)}
                        />
                        <View>
                            <Checkbox.Item label="เช็คเมื่อคุณเป็นเจ้าของอู่" status={checkbox} onPress={() => onCheckbox()} />
                        </View>
                    </View>
                    <View style={{ margin: 10, width: "100%", alignItems: "center" }}>
                        <Button style={{ width: "40%" }} icon="account-arrow-right" color="black" mode="contained" onPress={() => onSubmit()}>
                            Submit
                        </Button>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>


    )
}