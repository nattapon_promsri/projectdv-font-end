import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, TouchableOpacity, ImageBackground } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { useSelector } from 'react-redux'
import { getOneGarage } from '../api'
import { Card, Title, Paragraph, IconButton } from 'react-native-paper'
import { Header, Left, Body, Right, } from 'native-base';

export default function YourGarage(props) {


    const token = useSelector(state => state.token)

    const [oneGarage, setOneGarage] = useState([])

    useEffect(() => {
        getOneGarage(token)
            .then(res => {
                setOneGarage(res.data)
            })
            .catch(err => {
                console.log(err);

            })
    }, [])

    return (
        <View style={{ flex: 1, backgroundColor: "#ABB2B9" }}>
            <ImageBackground source={require('../assets/bg3.png')} style={{ height: 620, width: 370 }}>
                <Header style={{ backgroundColor: "white" }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack()}
                        >
                            <IconButton
                                icon="arrow-left-bold"
                                color="black"
                                size={40}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ alignItems: "center" }}>
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>Your Garage</Text>
                    </Body>
                    <Right>

                    </Right>
                </Header>
                <ScrollView>
                    <View style={{ flexDirection: "row" }}>
                        <FlatList
                            data={oneGarage}
                            renderItem={({ item }) => (
                                <View >
                                    <Card style={{ margin: 10 }}>
                                        <Card.Content>
                                            <Title>{item.name}</Title>
                                            <Paragraph>{item.description}</Paragraph>
                                        </Card.Content>
                                    </Card>
                                </View>
                            )}
                        />
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>

    )
}